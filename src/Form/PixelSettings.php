<?php

/**
 * @file
 * Contains \Drupal\audience_pixel\Form\PixelSettings.
 */

namespace Drupal\audience_pixel\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

class PixelSettings extends ConfigFormBase {
  public function getFormId() {
    return 'audience_pixel_admin_settings_form';
  }
  public function getEditableConfigNames() {
    return [
      'audience_pixel.settings',
    ];

  }
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = \Drupal::config('audience_pixel.settings');

    $form['account'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Audience.to Pixel settings'),
    );

    $form['account']['audience_pixel_app_id'] = array(
      '#title' => $this->t('App ID'),
      '#type' => 'textfield',
      '#default_value' => $config->get('audience_pixel_app_id'),
      '#size' => 40,
      '#required' => TRUE,
    );
    
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

    $userInputValues = $form_state->getUserInput();
    $config = $this->configFactory->getEditable('audience_pixel.settings');
    $config->set('audience_pixel_app_id', $userInputValues['audience_pixel_app_id']);
    $config->save();

    parent::submitForm($form, $form_state);
  }
}
